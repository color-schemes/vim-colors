# vim-color-schemes

![vim.png](https://i.imgur.com/40IZOhn.png)

**Installation:**

```
cd ~
git clone https://gitlab.com/ProjektRoot/recbox-config.git
cd vim-color-schemes/
cp manjaro_matcha_dark.vim ~/.vim/colors/
```
